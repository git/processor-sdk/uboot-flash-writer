#!/bin/bash

BASEDIR=$(git rev-parse --show-toplevel)
while [ $# -gt 0 ]; do
case $1 in
    --platform=*)
        platform="${1#*=}"
        ;;
    --sdk=*)
        TI_SDK_PATH="${1#*=}"
        ;;
esac
shift
done

if [ -z $platform ] || [ ! -f $BASEDIR/configs/platforms/$platform.cfg ] ; then
    echo "Error: invalid platform"
    exit 1
fi

[ -z $TI_SDK_PATH ] && TI_SDK_PATH=$BASEDIR/pkg_${platform}_tisdk-installer
mkdir -p $TI_SDK_PATH
cd $TI_SDK_PATH

## Add all required tools

TOOLS_LIST=$(sed -n "s|TOOLS?=||p" $BASEDIR/configs/platforms/$platform.cfg)

for comp in $TOOLS_LIST;
do
    echo $comp
	rsync -avu --progress $BASEDIR/tools/$comp $TI_SDK_PATH --exclude bin
	mkdir $TI_SDK_PATH/$comp/bin
	cp -r $BASEDIR/tools/$comp/bin/$platform $TI_SDK_PATH/$comp/bin
done
