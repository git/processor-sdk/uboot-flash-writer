# U-Boot Flash Writer

U-Boot Flash Writer are tools used to flash binaries to the on-board flash memory. 

## Directory Structure 

* tools - contains al the tools.
* configs - contains all config files for platforms.
* scripts - contains script to package tools for platforms.

## Tools

These tools can be found in tools directory

* DFU_flash
* Ethernet_flash
* uart_uniflash

Readme for tools can be found in the respective directories.

## Package using script

```
./scripts/create-sdk-pkg.sh --sdk=<path to sdk>/bin  --platform=<platform>
```

Currently supported `<platform>`s are:
- am62xx-evm, am62xx-lp-evm, am62xxsip-evm, am62pxx-evm
- am64xx-evm

For list of supported `<tools>`s for a `<platform>`, please look at ./config/`<platform>`.cfg
